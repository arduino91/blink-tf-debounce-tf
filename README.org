* Knižnica /blink-tf/ a /debounce-tf/

** Účel

Jedná sa o zjednodušenie kódu pre Arduino/IndustrialShields, ak sa riešia tieto situácie:

1. Potreba cyklicky aktivovaného výstupu (napr. blikanie indikačnými svetlami).
2. Softvérové ošetrenie zákmitov vstupu (napr. tlačidla).
3. Potreba aktivity až po nastaviteľnom trvaní digitálneho vstupu (napr. ak musí snímač (fotobunka,…) aktivovaný nejaký nastaviteľný čas).
\\

** Inštalácia

1. Stiahnuť repozitár pomocou tlačidla "šípka nadol" (vedľa /Clone/) alebo pomocou =git clone https://gitlab.com/arduino91/blink-tf-debounce-tf.git=
2. Adresáre =blink-tf= a =debounce-tf= nakopírovať do adresára s knižnicami pre Arduino IDE (typicky =<adresár s projektami>/libraries=).
\\

** Použitie
V podadresári =example= je príklad s názvom =blink_debounce_using_libs=. V ňom sa nachádza jednoduchý príklad použitia.
\\
V skratke sa jedná o tieto príkazy:

*** Pridanie knižníc:
#+BEGIN_SRC c
#include <blink-tf.h>
#include <debounce-tf.h>
#+END_SRC

\\

*** Inicializácia objektov:
#+BEGIN_SRC c
// Inicializácia: <názov>(<PIN>, <ČAS milisek.>)
// takže na pine č. 8 a č. 9 sú LEDky, ktoré budú blikať v nastavenom intervale
Blink zeleneSvetlo(8, 1000);
Blink cerveneSvetlo(9, 2000);

// Inicializácia: <názov>(<PIN>, <ČAS milisek>, <true|false>)
// ak <false> potom klasický INPUT (automaticky pinMode)
// ak <true> potom INPUT_PULLUP    (automaticky pinMode)
// ak tento parameter času chýba, tak je tam predvolene 50 ms
// "true" znamená, že vstup je typu INPUT_PULLUP
Debounce fotobunka(2, 2000, false);
#+END_SRC

\\

*** Príkazy:

=<názov>.On()=

Trvalo aktivuje výstup.
(napr.: =zeleneSvetlo.On();=) \\

\\

=<názov>.Off()=

Trvalo deaktivuje výstup. 
(napr. =zeleneSvetlo.Off();=)\\

\\

=<názov>.Update()=

Cyklicky vypína a zapína výstup.
(napr. =cerveneSvetlo.Update();=)\\

\\

=<názov>.SetInterval(int ms)=

Nastavuje čas v milisekundách, ktorý definuje čas aktivácie a čas deaktivácie výstupu pri použití /<názov>.Update/ resp. min. čas aktivácie vstupu pri =<názov>.read()=.
(napr. =fotobunka.SetInterval(2000);=) \\

\\

=<názov>.read()=

Nadobúda hodnotu *HIGH/true*, ak je nepretržite aktivovaný vstup po dobu nastavenú časom /<názov>.SetInterval/.
(napr. =if (fotobunka.read()) <rob_niečo> ;)=\\

\\

*** Príkazy, ktoré nie sú v príklade:

=<názov>.count()=

Vracia počet aktivovaní vstupu.\\

\\

=<názov>.resetCount()=

Resetuje počítadlo /<názov>.count()/.\\

\\

** Konkrétny príklad z praxe - blink-tf

Jedná sa o riadenie procesov v potravinárskej linke, na ktorej práve pracujeme.\\

V časti definícií mám:
#+BEGIN_SRC 
Blink SvetloSkrabanie(pinSvetloGN, 1000);

enum StavySvetiel {ON, OFF, BLINK_SLOW, BLINK_FAST};
enum StavySvetiel stavSvetloSkrabanie;

#+END_SRC

Tým sa vytvorí objekt *SvetloSkrabanie*, výstup je na pine *pinSvetloGN*, a predvolene bude blikať so zmenou HIGH/LOW po 1000 ms. Enumenátory *StavySvetiel* sú definované preto, lebo sa používajú v ďalších svetlách signalizačného stĺpika.

\\

Ďalej mám niekde túto funkciu:
#+BEGIN_SRC 
void svetloSkrabanie() {
  // 0 = OFF, 1 = ON, 2 = SLOW, 3 = FAST
  if (stavSvetloSkrabanie == OFF) SvetloSkrabanie.Off();
  if (stavSvetloSkrabanie == ON) SvetloSkrabanie.On();
  if (stavSvetloSkrabanie != ON && stavSvetloSkrabanie != OFF) {
    if (stavSvetloSkrabanie == BLINK_SLOW)
      SvetloSkrabanie.SetInterval(BLIK_P);
    if (stavSvetloSkrabanie == BLINK_FAST)
      SvetloSkrabanie.SetInterval(BLIK_R);
    SvetloSkrabanie.Update();
  }
}
#+END_SRC
\\

V nej sa definujú rôzne stavy, ako to svetlo môže blikať (vypnuté, zapnuté trvalo, blikajúce pomaly a rýchlo).\\

A nakoniec niekde v tele samotného programu je iba priradenie zo zoznamu definovaného v *enum*, ktorý hovorí, ako sa má blikať (podľa podmienok a stavu strojov):
#+BEGIN_SRC 
stavSvetloSkrabanie = BLINK_FAST;
#+END_SRC
\\
A potom samotná aktivácia výstupov je zavolaním funkcie:
#+BEGIN_SRC 
svetloSkrabanie();  
#+END_SRC
\\
Riešenie s /<názov>.Update()/ nepoužíva časové prerušenia (ISR) - to bude v ďalšej verzii - preto je výhodné riešiť hocijaké úlohy ako stavové automaty, a pokiaľ majú vyšší stupeň zloženosti, tak volanie funkcie musí byť v každej vetve.

\\

** Konkrétny príklad z praxe - debounce-tf

Zase - vytvorenie objektu:

#+BEGIN_SRC 
Debounce PinFotobunka(pinFotobunkaZP, 5000, false);
#+END_SRC
\\
/pinFotobunkaZP/ je pin na ktorom je pripojený snímač, /5000/ je defaultný čas, neskôr zmenený pomocou SetInterval, a /false/ znamená, že čítame z pinu HIGH stav ako relevantný.

\\

\\
Funkcia pre čítanie zo vstupu:
#+BEGIN_SRC 
int fotobunka() {
  // NO kontakt: ak je aktivov. => HIGH
  // výstup až po minim. čase aktivovania
  return (PinFotobunka.read()) ? 1 : 0;
}
#+END_SRC
\\

A niekde v tele vykonávacej časti je nastavenie intervalu (min. času signálu):
#+BEGIN_SRC 
PinFotobunka.SetInterval(map(analogRead(pinR2), 0, 512, 0, MAX_R2));
#+END_SRC
\\


** Zdroje
Jedná sa o upravené pôvodné myšlienky a kódy:

https://github.com/brakdag/blink

https://github.com/wkoch/Debounce
***** Posledná úprava: <2022-11-09 St>
