/* original idea by Gustavo Ferreyra
   improvement by Richard Fabo
*/

#ifndef BLINK-TF_H_INCLUDED
#define BLINK-TF_H_INCLUDED

class Blink
{
public:
  Blink(int pin, int time);
  void Update();
  void Off();
  void On();
  void SetInterval(int time);
  
private:
  int _intervalo;
  int _pin;
  int _led_estado;
  unsigned long _tiempo_anterior;
};

#endif // BLINK-TF_H_INCLUDED
