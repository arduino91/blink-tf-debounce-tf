/* 
  Zmena výstupu a aktivovanie vstupu po čase trvania

  Knižnice
    blink-tf.h
    debounce-tf.h

  Výstupy "LED" majú automaticky príznak OUTPUT.

  Po stlačení tlačidla na pine č. 2 sa interval blikania zmenší o 1/2.
  Po stlačení tlačidla na pine č. 3 a jeho aktivácii nad xxxx ms
   blikne LED_BUILTIN

*/


#include <blink-tf.h>
#include <debounce-tf.h>

int startingValueLED1 = 2000;
int startingValueLED2 = 4000;
int startingValueDebounce = 2000;
byte pinButtonTimer = 2;
byte pinButtonDebounce = 3;

// Inicializácia: <názov>(<PIN>, <ČAS milisek.>);
Blink led1(8, startingValueLED1);
Blink led2(9, startingValueLED2);

// Inicializácia: názov(<PIN>, <ČAS milisek>, <true|false>)
// ak <false> potom klasický INPUT (automaticky pinMode)
// ak <true> potom INPUT_PULLUP    (automaticky pinMode)
Debounce pinDebounce(pinButtonDebounce, startingValueDebounce, true);


void setup() {
  pinMode(pinButtonTimer, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  if (digitalRead(pinButtonTimer) == LOW) {
    startingValueLED1 = startingValueLED1 / 2;
    startingValueLED2 = startingValueLED2 / 2;
    startingValueDebounce = startingValueDebounce / 2;
    if (startingValueLED1 < 25) {
    	startingValueLED1 = 2000;
    	startingValueLED2 = 4000;
	startingValueDebounce = 2000;
    }
    for (byte i = 0; i < 4 ; i++) {
      led1.On();
      led2.On();
      delay(50);
      led1.Off();
      led2.Off();
      delay(50);
    }
    led1.SetInterval(startingValueLED1);
    led2.SetInterval(startingValueLED2);
    pinDebounce.SetInterval(startingValueDebounce);
  }

  
  led1.Update(); 
  led2.Update();
  
  if (pinDebounce.read()) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);    
  }
}

